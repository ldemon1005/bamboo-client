FROM registry.gitlab.com/cmc-si/loyalty-system/client-service-base:1.0
MAINTAINER Slowlove

COPY ./cordova/www /var/client/cordova/www

EXPOSE 22 3001

WORKDIR /var/client/cordova/www
CMD ["nginx", "-g", "daemon off;"]