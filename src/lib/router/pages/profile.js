import u from '../../utilities/utility'

const routers = u.load({
  name: 'profile',
  pages: {
    detail: {
      p: '/profile',
      n: 'Hồ sơ người dùng'
    }
  }
});

export default {
  routers,
  router: {
    path: 'profile',
    redirect: '/profile-web',
    name: 'Người dùng',
    component: {
      render (c) {
        return c('router-view')
      }
    },
    children: [
      routers.detail
    ]
  }
}
