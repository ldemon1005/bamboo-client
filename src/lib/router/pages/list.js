import u from '../../utilities/utility'

const routers = u.load({
    name: 'pages',
    pages: {
        listProduct: {
            p: '/list-product',
            n: 'Danh sách sản phẩm',
            f: 'listProduct',
        },
    }
});

export default routers
