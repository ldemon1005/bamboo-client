import u from '../../utilities/utility'

const routers = u.load({
  name: 'config',
  pages: {
    config_edit: {
      p: '/config/:uid/edit',
      f: 'config/edit',
      n: 'Sửa cấu hình'
    },
    config_detail: {
      p: '/config/:uid',
      f: 'config/detail',
      n: 'Chi tiết cấu hình'
    }
  }
})

export default {
  routers,
  router: {
    path: '/',
    name: 'Hệ Thống',
    component: {
      render (c) {
        return c('router-view')
      }
    },
    children: [
      routers.config_edit,
      routers.config_detail      
    ]
  }
}
