import u from "../../utilities/utility";

const routers = u.load({
    name: "pages",
    pages: {
        client_web: {
            p: "/client-web",
            n: "client-web",
            f: "client-web"
        },
        client_history_web: {
            p: "/client-history-web",
            n: "client-history-web",
            f: "client-history-web"
        },
    }
});

export default routers;
