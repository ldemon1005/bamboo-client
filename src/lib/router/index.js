import Vue from "vue";
import Router from "vue-router";
import Page from "./pages/pages";
import Container from "../../App";

Vue.use(Router);


const routing = new Router({
    mode: "hash",
    linkActiveClass: "open active",
    scrollBehavior: () => ({y: 0}),
    routes: [
        {
            path: '/website',
            name: 'website',
            component: Container,
            children: [
                Page.client_web,
                Page.client_history_web,
            ]
        },
        {
            path: "*",
            redirect: "/client-web",
            name: "Trang Chủ",
            component: Container,
            children: []
        }
    ]
});

export default routing;
