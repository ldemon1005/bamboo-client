import Vue from 'vue'
import Vuex from 'vuex'
import State from './state'
import Mutations from './mutations'
import Getter from './getters'
import Actions from './actions'
import EasyAccess from 'vuex-easy-access'
import Modules from './modules'

Vue.use(Vuex)

export default new Vuex.Store({
  State,
  Mutations,
  Getter,
  Actions,
  plugins: [EasyAccess()],
  modules: Modules
})
