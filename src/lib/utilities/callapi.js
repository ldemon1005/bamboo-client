import authentication from "./authentication";
import axios from 'axios'
import Vue from 'vue'
require('dotenv').config();
let _ = require('lodash');

Vue.use(axios);
var baseUrl = process.env.VUE_APP_BASE_URL;
const post = (link, auth = false, data = {}) => {
    console.log(data);
    if(auth && !authentication.isLoggedIn()){
        window.location = '/login';
    }
};

const get =  (link, auth = false, params) => new Promise((resolve, reject) => {
    let config = {};
    if(auth && !authentication.isLoggedIn()){
        window.location = '/login';
    }else {
        config = {
            headers: {
                Authorization: localStorage.getItem('__ck')
            },
        };
    }
    let url_params = '';
    let url = '';
    if(!_.isEmpty(params)){
        url = baseUrl + link + "&" + url_params;
    }else {
        url = baseUrl + link;
    }
    axios.get(url,config).then((res) => {
        let result = res.data;
        if (result.code && result.code !== 200) {
            this.$q.notify({
                color: 'red',
                textColor: 'white',
                icon: 'thumb_up',
                message: `Không thể truy xuất được dữ liệu từ máy chủ vì lỗi:\n\n*${result.message}*`,
                position: 'top-right',
                timeout: 3000
            });
            reject(result)
        } else {
            resolve(result)
        }
    })
})

export default {
    get,
    post
}
