let m = require("moment");
import u from "../utilities/utility";
import { isDate } from "moment";
import Authentication from '../utilities/authentication'
import db from './database'
export default {
  data() {
    return {
      limitItems: 5,
      imageDefault:
        "https://beyondphilosophy.com/wp-content/uploads/2017/07/Revealed-How-To-Make-Loyalty-Easy-Colin-Shaw-Featured-Image.jpg",
      imageCustomer:
        "https://bluemountain.vn/wp-content/uploads/2017/07/nh%C3%A2n-vi%C3%AAn-ch%C4%83m-s%C3%B3c-kh%C3%A1ch-h%C3%A0ng.jpg",
      phoneCustomer: "0975674110",
      listTypeTransaction: typesTran,
      offerType : SCREENS,
      VOUCHER : 2,
      EVENT:2,
      NEWS:0

    };
  },
  methods: {
    showConfirm: async function(message, title) {
      var result = false;
      var fc = this.$q.dialog({
        title: title,
        message: message,
        ok: this.$t("common.Ok"),
        cancel: this.$t("common.cancel")
      });
      await fc
        .then(() => {
          result = true;
        })
        .catch(() => {
          result = false;
        });
      return result;
    },
    getGenderLabel: function(value) {
      var result = "N/A";
      gender.forEach(item => {
        if (value == item.value) {
          result = this.$t(item.label);
        }
      });
      return result;
    },
    getValueFieldObj(obj, field) {
      let result = null;
      if (!checkEmptyValue(obj)) {
        if (!checkEmptyValue(obj[field])) {
          result = obj[field];
        }
      }
      return result;
    },
    getLinkImage(obj, field) {
      let result = "";
      try {
        if (!checkEmptyValue(obj[field])) {
          if (field !== "icon") {
            result = baseUrl + obj[field];
          } else {
            result = obj[field];
          }
        }
      } catch (e) {
        result = "";
      }
      return result;
    },
    showError: function(message = null) {
      if (checkEmptyValue(message)) {
        message = messError;
      }
      this.$q.dialog({
        color: "red",
        textColor: "white",
        message: message,
        timeout: 3000
      });
    },
    showSuccess: function() {
      this.$q.notify({
        color: "blue",
        textColor: "white",
        icon: "thumb_up",
        message: "Success",
        position: "top-right",
        timeout: 3000
      });
    },
    checkEmpty: function(value) {
      return checkEmptyValue(value);
    },
    parseDateToString: function(value) {
      let result = "";
      try {
        if (value instanceof Date) {
          result = m(value).format(formatDate);
        } else {
          let date = new Date(value);
          if (isDate(date)) {
            result = m(date).format(formatDate);
          }
        }
      } catch (error) {
        return "";
      }
      return result;
    },
    parseDateTimeToString: function (value) {
      let result = "";
      try {
        if (value instanceof Date) {
          result = m(value).format(formatDateTime);
        } else {
          let date = new Date(value);
          if (isDate(date)) {
            result = m(date).format(formatDateTime);
          }
        }
      } catch (error) {
        return "";
      }
      return result;
    },
    getTransactionLabel: function(value) {
      var result = "N/A";
      typesTran.forEach(item => {
        if (value == item.value) {
          result = item.label;
        }
      });
      return result;
    },
    setUserInfo: async function(){
        let result ={}
        let  user = {}
        let points = {}
        await  u.g("api/user/profile")
            .then(res => {
                user = res.data;
                points = {}
                user.rankName = this.getValueFieldObj(res.data.rank,'title')
                user.image = this.getLinkImage(res.data.avatar,'thumbLink')
                user.rankImage = this.getLinkImage(user.rank.image,'thumbLink')
                        if(!this.checkEmpty(res.data.points)){
                            points = res.data.points
                            points.forEach(e=>{
                                if(e.name === 'Rank Point'){
                                    user.rankPoint = e.point
                                }
                            })
                        }
                })
        db.l.s('userInfo', JSON.stringify(user))
        result.user = user
        result.points = points
        return result;
    },
    getUserInfo(){
        let user =  Authentication.getUserInfo();
        user.rankName = this.getValueFieldObj(user.rank,'title')
        user.image = this.getLinkImage(user.avatar,'thumbLink')
        user.fullName = user.firstname+" " +user.lastname
        user.rankImage = this.getLinkImage(user.rank.image,'thumbLink')
        user.rankPoint = 0
        if(!this.checkEmpty(user.points)){
            user.points.forEach(e=>{
                if(e.name === 'Rank Point'){
                    user.rankPoint = e.point
                }
            })
        }
        return user
    }
      
  }
};
const gender = [
  { value: 1, label: "common.gender.male" },
  { value: 2, label: "common.gender.fmale" }
];
const baseUrl = u.baseUrl;
const messError = "Đã xảy ra lỗi ."
const formatDate = "MM/DD/YYYY";
const formatDateTime = "HH:MM:SS MM/DD/YYYY";
function checkEmptyValue(value) {
  let result = false;
  if (value instanceof Array) {
    if (value.length == 0) {
      result = true;
    }
  }
  if (value instanceof Object) {
    if (Object.getOwnPropertyNames(value).length === 0) {
      result = true;
    }
  }
  if (value == undefined || value == null || value === '') {
    result = true;
  }
  return result;
}
const typesTran = [
  { value: 0, label: "Hoạt động không ảnh hưởng tới điểm" },
  { value: 1, label: "Nạp điểm" },
  { value: 2, label: "Tiêu điểm" }
];
const SCREENS = [
  { display: "Info", label: "information", value: 0 },
  { display: "Gift", label: "gift", value: 1 },
  { display: "Voucher", label: "voucher", value: 2 },
  { display: "Coupon", label: "coupon", value: 3 },
  { display: "Reward", label: "reward", value: 4 },
  { display: "Event", label: "event", value: 5 },
  { display: "Combo", label: "bonus", value: 6 }
];