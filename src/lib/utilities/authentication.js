// import auth0 from 'auth0-js'
// import Router from 'vue-router'
// import decode from 'jwt-decode'
import cookies from 'vue-cookies'
import u from './utility'
import db from './database'

const SESSION_STATUS = 'ss'
const SESSION_DATA = '__as'
const SESSION_USER = '__uf'
const SESSION_INFO = '__if'
const USERS_COOKIE = '__ck'
const TOKEN_KEY_ID = '__ast'
const REDIRECT = 'login'
const REDIRECT_WEB = 'index'
const ACCESS_TOKEN = 'access_token'

const outpages = [
    '/register',
    '/login',
    '/active',
    '/forgot',
    '/error',
    '/confirm',
    '/resetPassword',
];
// const SCOPE = ''
// const AUDIENCE = ''

// var auth = new auth0.WebAuth({
//   clientID: CLIENT_ID,
//   domain: CLIENT_DOMAIN
// })

// var router = new Router({
//   mode: 'history'
// })

const getIdToken = () => cookies.get(TOKEN_KEY_ID)

const getAccessToken = () => localStorage.getItem(ACCESS_TOKEN)
const getExternalUserId = () => localStorage.getItem('externalUserId')
const getExpried = () => localStorage.getItem('expired')
const getUserInfo = () => JSON.parse(localStorage.getItem('userInfo'))
const getBrandInfo = () => JSON.parse(localStorage.getItem('brandInfo'))

// function clearIdToken () {
//   cookies.remove(TOKEN_KEY_ID)
// }

// function clearAccessToken () {
//   localStorage.removeItem(ACCESS_TOKEN)
// }
function getParameterByName(name) {
    let match = RegExp('[#&]' + name + '=([^&]*)').exec(window.location.hash)
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '))
}

function setAccessToken() {
    let accessToken = getParameterByName(ACCESS_TOKEN)
    localStorage.setItem(ACCESS_TOKEN, accessToken)
}

function setIdToken() {
    let idToken = getParameterByName('id_token')
    localStorage.setItem(ACCESS_TOKEN, idToken)
}

// function getTokenExpirationDate (encodedToken) {
//   const token = decode(encodedToken)
//   if (!token.exp) {
//     return null
//   }
//   const date = new Date(0)
//   date.setUTCSeconds(token.exp)
//   return date
// }

// const isTokenExpired = (token) => getTokenExpirationDate(token) < new Date()

const isLoggedIn = () => {
    const cookie = db.l.g(USERS_COOKIE)
    // const key = db.c.g(TOKEN_KEY_ID)
    // const dat = db.l.g(SESSION_DATA)
    // const usr = db.l.g(SESSION_USER)
    // const inf = db.l.g(SESSION_INFO)
    // return dat && dat.length > 10 && key && key.length > 10 && usr && usr.length > 10 && inf && inf.length > 0
    return cookie && cookie.length > 10
}

/* eslint-disable */
function forbidden(to) {
    const session = u.session()
    const urlpath = to.path.replace('/', '')
    return to.path === '/' || to.path === '/dashboard' ? false : !u.is.in(session.roles, urlpath)
}

function login(obj, data, link) {
    db.c.s(TOKEN_KEY_ID, data.access_token)
    db.l.s(USERS_COOKIE, data.access_token)
    db.l.s(SESSION_STATUS, 'started')
    db.l.s('externalUserId', data.user.code)
    db.l.s('userInfo', JSON.stringify(data.user))
    db.l.s('expired', data.exp)
    // db.l.s(SESSION_DATA, JSON.stringify(data.roles))
    // db.l.s(SESSION_INFO, JSON.stringify(data.extra))
    // db.l.s(SESSION_USER, JSON.stringify(data.information))

    obj.$router.push(link)
}

function logout(obj) {
    u.p('api/user/logout').then(r => {})
    db.l.s(SESSION_STATUS, '')
    db.l.s(SESSION_DATA, '?')
    db.l.s(SESSION_USER, '?')
    db.l.s(SESSION_INFO, '?')
    db.l.d()
    db.c.r(TOKEN_KEY_ID)

    // obj.push(REDIRECT)
}

function checkAuthorize(to, from, next) {
    if (u.is.has(to.query, 'messenger')) {
        const content = to.query.messenger.split(':')
        const action = content[0]
        const detail = content[1]
        let msg = ''
        if (action === 'alert') {
            msg = detail
        }
        if (msg) {
            alert(msg)
        }
    }
    if (!isLoggedIn()) {
        if (outpages.indexOf(to.path) === -1) {
            cookies.remove(TOKEN_KEY_ID);
            if( screen.width <= 760 ) {
                next({path: REDIRECT});
            }
            else {
                next({path: REDIRECT_WEB});
            }
        } else {
            next()
        }
    } else {
        if(this.getExpried() < Date.now() / 1000){
            u.p('api/user/extend').then(res => {
                let linkRouter= '';
                if( screen.width <= 760 ) {
                    linkRouter ='homescene';
                }
                else {
                    linkRouter ='index';
                }
                login(this, res, linkRouter);
            }).catch(err => {
                console.log(err);
            })
        }
        next()
    }
};

export default {
    login,
    logout,
    checkAuthorize,
    isLoggedIn,
    setIdToken,
    setAccessToken,
    getIdToken,
    getAccessToken,
    getExternalUserId,
    getExpried,
    getUserInfo,
    getBrandInfo,
}
