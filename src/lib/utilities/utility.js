/* eslint-disable */
import Vue from 'vue'
import md5 from 'js-md5'
import Axios from 'axios'
import db from './database'
import moment from 'moment'
import CryptoJS from 'crypto-js'
import {isUndefined, isNullOrUndefined, isString} from 'util'
import lodash from 'lodash'

require('dotenv').config();

Vue.prototype.moment = moment
Vue.prototype.crypt = CryptoJS

const code_http = {
    processing: 102,
    ok: 200,
    completed: 201,
    done: 202,
    created: 204,
    missing_parameters: 400,
    unauthorized: 401,
    forbidden: 403,
    underfined: 404,
    not_allowed: 405,
    request_expired: 408,
    conflict: 409,
    unprocessable: 422,
    resent: 429,
    fail: 500,
    exception: 501,
    unsavedb: 502,
    unavailable: 503
}

function log (...obj) {
    console.time('Duration')
    if (obj.length === 1) {
        const cnt = isString(obj[0]) ? obj[0] : JSON.stringify(obj[0])
        console.log("\n%c"+prl(), "color:DodgerBlue")
        console.info("%c" + cnt, "color:DodgerBlue")
        console.timeEnd('Duration')
        console.log("%c"+prl()+"\n", "color:DodgerBlue")
    } else if (obj.length === 2) {
        const msg = obj[0].toString().toUpperCase()
        let cnt = obj.slice(1)
        // console.log(`\n${prl()}\n${msg}: ${cnt}\n${prl()}\n\n`)
        cnt = isString(cnt) ? cnt : JSON.stringify(cnt)
        console.log("\n%c"+prl(), "color:DodgerBlue")
        console.groupCollapsed(msg)
        console.info("%c" + cnt, "color:DodgerBlue")
        console.timeEnd('Duration')
        console.groupEnd(msg)
        console.log("%c"+prl()+"\n", "color:DodgerBlue")
    } else {
        const msg = obj[0].toString().toUpperCase()
        const arr = obj.slice(1)
        if (arr) {
            let i = 0
            console.log("\n%c"+prl('='), "color:Green")
            console.group(msg)
            console.info("%c"+prl('˜'),"color:Green")
            arr.map(o => {
                i += 1
                console.groupCollapsed(`Item ${i}`)
                if (_.isObject(o)) {
                    console.log(`${JSON.stringify(o)}`)
                } else if (isString(o)) {
                    console.log(`${o}`)
                } else {
                    console.log(o)
                }
                if (i < arr.length - 1) {
                    console.log(`${prl('-')}`)
                }
                console.groupEnd(`Item ${i}`)
                return o
            })
            console.log("%c"+prl('_'),"color:Green")
            console.warn(`Total: ${arr.length}`)
            console.timeEnd('Duration')
            console.groupEnd(msg)
            console.log("%c"+prl()+"\n", "color:Green")
        }
    }
}

const lg = (...obj) => {
    let val = null
    let lbl = 'Variable'
    let cfg = 0
    if (obj.length === 1) {
        val = obj[0]
    } else if (obj.length === 2) {
        lbl = obj[0].toString().toUpperCase()
        val = obj[1]
    } else {
        lbl = obj[0].toString().toUpperCase()
        cfg = obj[(obj.length - 1)]
        val = obj.slice(1)
        if (!isNaN(cfg) && parseInt(cfg, 10) > 0 && parseInt(cfg, 10) < 5) {
            cfg = parseInt(cfg, 10)
            val = val.slice(-1)
        }
    }
    if (cfg === 4) {
        console.clear()
        console.trace()
    } else if (cfg === 3) {
        console.log("\n%c"+prl('=')+"\n"+lbl+"\n"+prl('.'), "color:Red")
        console.error(JSON.parse(JSON.stringify(val)))
        console.log("%c"+prl('=')+"\n", "color:Red")
    } else if (cfg === 2) {
        console.log("\n%c"+prl('=')+"\n"+lbl+"\n"+prl('-'), "color:DoubleBlue")
        console.info(JSON.parse(JSON.stringify(val)))
        console.log("%c"+prl('=')+"\n", "color:DoubleBlue")
    } else if (cfg === 1) {
        console.log("\n%c"+prl('=')+"\n"+lbl+"\n"+prl('-'), "color:Orange")
        console.warn(JSON.parse(JSON.stringify(val)))
        console.log("%c"+prl('=')+"\n", "color:Orange")
    } else {
        console.log("\n%c"+prl('=')+"\n"+lbl+"\n"+prl('-'), "color:Green")
        console.log(JSON.parse(JSON.stringify(val)))
        console.log("%c"+prl('=')+"\n", "color:Green")
    }
}

const config = {}

const c = env => {
    if (Object.entries(env).length > 0 && typeof env === 'object') {
        Object.entries(env).forEach(([key, val]) => {
            let k = key
            let v = val
            let p = ''
            let pref = ''
            let alias = key.substr(8)
            if (alias.substr(0, 5) === 'MEDIA') {
                pref = alias.substr(0, 5).toLowerCase()
                k = alias.substr(6).toLowerCase()
                const o = alias.substr(6).split('_')
                k = o[0].toLowerCase()
                p = o[1].toLowerCase()
                v = {}
                if (alias.substr(-4) === 'TYPE') {
                    v[p] = val.split(',')
                } else if (alias.substr(-4) === 'SIZE') {
                    v[p] = parseInt(val)
                } else {
                    v[p] = isNaN(val) ? val : parseInt(val)
                }
            } else {
                pref = 'app'
                k = key.substr(8)
                k = k.substr(0, 3) === 'APP' ? k.substr(4).toLowerCase() : k.toLowerCase()
                v = isNaN(val) ? val : parseInt(val)
            }
            if (pref && k) {
                config[pref] = config[pref] && typeof config[pref] === 'object' ? config[pref] : {}
                if (typeof v === 'object') {
                    config[pref][k] = config[pref][k] && typeof config[pref][k] === 'object' ? config[pref][k] : {}
                    config[pref][k][Object.keys(v)[0]] = v[Object.keys(v)[0]]
                } else {
                    config[pref][k] = typeof v === 'object' ? v : v
                }
            } else if (k && v) {
                config[k.toLowerCase()] = v
            }
        })
    }
}

const cfg = (obj, val = null) => {
    if (typeof obj === 'string' && val !== null) {
        config[obj] = val
    } else if (typeof obj === 'object' &&  Object.entries(obj).length > 0 && val === null) {
        Object.entries(obj).forEach(([key, val]) => {
            config[key] = val
        })
    }
}

c(process.env)

const baseUrl = config.app.service_api

const SESSION_DATA = '__as'
const SESSION_USER = '__uf'
const SESSION_INFO = '__if'
const USERS_COOKIE = '__ck'
const TOKEN_KEY_ID = '__ast'

let temp = {}
temp = {
    calling: {}
}

const o = new Vue()

const bus = new Vue()

const v = {
    img: {
        size: config.media.img.size,
        exts: config.media.img.type
    },
    file_img: {
        size: config.media.img.size,
        exts: config.media.img.type
    },
    doc: {
        size: config.media.doc.size,
        exts: config.media.doc.type
    },
    transfer_file: {
        size: config.media.file.size,
        exts: config.media.file.type
    },

    check(t, ft) {
        let resp = 'not_valid'
        const type = t.toString().replace('.', '').toLowerCase()
        if ((this.img.exts.indexOf(type) > -1) && (ft === 'img')) {
            resp = 'img'
        }
        if ((this.doc.exts.indexOf(type) > -1) && (ft === 'doc')) {
            resp = 'doc'
        }
        if ((this.transfer_file.exts.indexOf(type) > -1) && (ft === 'transfer_file')) {
            resp = 'transfer_file'
        }
        return resp
    }
}

const n = (k, v = null) => process.env[`VUE_APP_${k.toString().toUpperCase()}`] ? process.env[`VUE_APP_${k.toString().toUpperCase()}`] : v

const ada = {
    c: () => Vue.prototype.global = {},
    s: (k, v = null) => {
        if (isUndefined(Vue.prototype.global)) {
            Vue.prototype.global = {}
        }
        Vue.prototype.global[k] = v
    },
    i: (m, f = null) => Vue.prototype.global[m] = f,
    g: (k, v = null) => isUndefined(Vue.prototype.global[k]) ? v : Vue.prototype.global[k],
    r: (k) => !isUndefined(Vue.prototype.global[k]) ? delete Vue.prototype.global[k] : false,
    d: (m, p = null) => !isUndefined(Vue.prototype.global[m]) ? Vue.prototype.global[m](p) : false
}

Vue.prototype.o = ada


const x = (widget, path) => {
    return {
        [widget]: () => import(`${path}`)
    }
}

const token = () => {
    let resp = db.c.g(TOKEN_KEY_ID)
    const valid = db.l.g(USERS_COOKIE)
    return resp && resp !== '' && resp === valid ? resp : valid ? valid : resp
};

const session = () => {
    return {
        roles: JSON.parse(db.l.g(SESSION_DATA)),
        user: JSON.parse(db.l.g(SESSION_USER)),
        info: JSON.parse(db.l.g(SESSION_INFO)),
    }
}

const chk = {
    boss: () => {
        const u = JSON.parse(db.l.g(SESSION_USER))
        return (u.role_id === RCM || u.role_id === REC) && u.superior_id === ''
    }
}

const ca = (act = '') => {
    let resp = false
    if (act) {
        const u = JSON.parse(db.l.g(SESSION_USER))
        if (u.role_id) {
            const role = parseInt(u.role_id, 10)
            resp = is.has(la, act) ? is.in(la[act], role) : resp
        }
    }
    return resp
}

const authorized = roleId => {
    const role = isNullOrUndefined(roleId) ? session().user.role_id : roleId
    return role > RBC;
}

const role = (id = 0) => {
    const uid = session().user && is.has(session().user, 'role_id') ? session().user.role_id : 0
    let rid = parseInt(id, 10) > 0 ? id : uid
    let resp = ''
    Object.keys(r).map(k => {
        if (parseInt(rid, 10) === parseInt(r[k], 10)) {
            resp = k
        }
    })
    return resp
}

const a = (unauthorize,environment = "baseUrl") => {
    const check = isNullOrUndefined(unauthorize) || unauthorize === '' || parseInt(unauthorize) === 0
    Axios.defaults.headers.post['Content-Type'] = 'application/json';
    Axios.defaults.headers.put['Content-Type'] = 'application/json';
    Axios.defaults.headers.common['x-scope'] = 'client';
    if(environment == "baseUrl"){
        Axios.defaults.baseURL = baseUrl;
    }
    if (check) {
        const tokenKey = token()
        if (tokenKey) Axios.defaults.headers.common['Authorization'] = tokenKey
    } else {
        if (is.has(Axios.defaults.headers.common, 'Authorization')) delete Axios.defaults.headers.common['Authorization']
        if (unauthorize === 2) Axios.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded'
    }
    return Axios
}

const g = (link, attributes = null, unauthorize = false, blob = false) => new Promise((resolve, reject) => {
    if (typeof link === 'string') {
        let hash = ''
        if (!unauthorize) {
            hash = md5(`${link}${JSON.stringify(attributes)}${moment().format('YYYY-MM-DD|HH:mm:ss')}`)
        }
        if (!live(temp.calling) || (live(temp.calling) && !is.has(temp.calling, hash))) {
            link = `${config.app.service_api}/${link}`
            if (hash) {
                temp.calling[hash] = 'processing...'
            }
            const payload = {
                params: attributes
            }
            if (blob) {
                payload.responseType = 'blob'
            }
            a().get(link, payload).then(response => {
                let result = response.data
                if (!unauthorize && live(temp.calling) && is.has(temp.calling, hash)) {
                    delete temp.calling[hash]
                }
                if (!blob && result.code && result.code !== 200) {
                    alert(`Không thể truy xuất được dữ liệu từ máy chủ vì:\n\n*${result.message}*`)
                    // error(result)
                    reject(result)
                } else {
                    resolve(result)
                }
            }).catch(e => {
                reject(e)
            })
        } else {
            log('Double request', temp.calling[hash])
            reject('Double request...')
        }
    } else {
        reject('Request url is not valid')
    }
})

const p = (link, params = null, environment = "baseUrl",unauthorize = false, full_response = false) => new Promise((resolve, reject) => {
    if (typeof link === 'string') {
        let hash = '';
        if (!unauthorize) {
            hash = md5(`${link}${JSON.stringify(params)}${moment().format('YYYY-MM-DD|HH:mm:ss')}`)
        }
        if (!live(temp.calling) || (live(temp.calling) && !is.has(temp.calling, hash))) {
            if (hash) {
                temp.calling[hash] = 'processing...'
            }
            link = `${config.app.service_api}/${link}`
            a().post(link, params).then(response => {
                const result = response.data;
                if (!full_response) {
                    const result = response.data;
                    if (result.code && result.code !== 200) {
                        alert(`Không thể truy xuất được dữ liệu từ máy chủ vì lỗi:\n\n*${result.message}*`)
                        // error(result)
                        reject(result)
                    } else {
                        resolve(result.data)
                    }
                } else {
                    resolve(result)
                }
            }).catch(e => {
                reject(e)
            })
        } else {
            log('Double request', temp.calling[hash])
            reject('Double request...')
        }
    } else {
        reject('Request url is not valid')
    }
});

const put = (link, params = null, unauthorize = false, full_response = false) => new Promise((resolve, reject) => {
    if (typeof link === 'string') {
        let hash = ''
        if (!unauthorize) {
            hash = md5(`${link}${JSON.stringify(params)}${moment().format('YYYY-MM-DD|HH:mm:ss')}`)
        }
        if (!live(temp.calling) || (live(temp.calling) && !is.has(temp.calling, hash))) {
            if (hash) {
                temp.calling[hash] = 'processing...'
            }
            link = `${config.app.service_api}/${link}`
            a().put(link, params).then(response => {
                const result = response.data
                if (!full_response) {
                    if (result.code && result.code !== 200) {
                        alert(`Không thể truy xuất được dữ liệu từ máy chủ vì lỗi:\n\n*${result.message}*`)
                        // error(result)
                        reject(result)
                    } else {
                        resolve(result.data)
                    }
                } else {
                    resolve(result)
                }
            }).catch(e => {
                reject(e)
            })
        } else {
            log('Double request', temp.calling[hash])
            reject('Double request...')
        }
    } else {
        reject('Request url is not valid')
    }
})

const t = (link, params, unauthorize = false) => new Promise((resolve, reject) => {
    if (typeof link === 'string') {
        let hash = ''
        if (!unauthorize) {
            hash = md5(`${link}${JSON.stringify(params)}${moment().format('YYYY-MM-DD|HH:mm:ss')}`)
        }
        if (!live(temp.calling) || (live(temp.calling) && !is.has(temp.calling, hash))) {
            if (hash) {
                temp.calling[hash] = 'processing...'
            }
            link = `${config.app.service_api}/${link}`
            a().put(link, params).then(response => {
                const result = response.data
                if (result.code && result.code !== 200) {
                    alert(`Không thể truy xuất được dữ liệu từ máy chủ vì lỗi:\n\n*${result.message}*`)
                    // error(result)
                    reject(result)
                } else {
                    resolve(result.data)
                }
            }).catch(e => {
                reject(e)
            })
        } else {
            log('Double request', temp.calling[hash])
            reject('Double request...')
        }
    } else {
        reject('Request url is not valid')
    }
})

const d = (link, unauthorize = false) => new Promise((resolve, reject) => {
    if (typeof link === 'string') {
        let hash = ''
        if (!unauthorize) {
            hash = md5(`${link}${moment().format('YYYY-MM-DD|HH:mm:ss')}`)
        }
        if (!live(temp.calling) || (live(temp.calling) && !is.has(temp.calling, hash))) {
            if (hash) {
                temp.calling[hash] = 'processing...'
            }
            link = `${config.app.service_api}/${link}`
            a().delete(link).then(response => {
                const result = response.data
                if (live(temp.calling) && is.has(temp.calling, hash)) {
                    delete temp.calling[hash]
                }
                if (result.code && result.code !== 200) {
                    alert(`Không thể truy xuất được dữ liệu từ máy chủ vì lỗi:\n\n*${result.message}*`)
                    // error(result)
                    reject(result)
                } else {
                    resolve(result.data)
                }
            }).catch(e => {
                reject(e)
            })
        } else {
            log('Double request', temp.calling[hash])
            reject('Double request...')
        }
    } else {
        reject('Request url is not valid')
    }
})

const e = (link) => new Promise((resolve, reject) => {
    if (typeof link === 'string') {
        const tokenKey = token()
        Axios.get(link, {
            headers: {
                Authorization: tokenKey
            },
            responseType: "blob"
        }).then(response => {
            resolve(response.data)
        }).catch(e => {
            reject(e)
        })
    } else {
        reject('Request url is not valid')
    }
})

const mobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i)
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i)
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i)
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i)
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i)
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows())
    }
}

const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

const is = {
    in: (obj, key) => obj && Array.isArray(obj) && key ? parseInt(obj.indexOf(key), 10) > -1 : false,
    obj: obj => typeof obj === 'object' && !Array.isArray(obj),
    arr: obj => Array.isArray(obj),
    has: (obj, key) => typeof obj === 'object' && !Array.isArray(obj) ? Object.hasOwnProperty.call(obj, key) : false,
    for: obj => Object.keys(obj),
    mobile: () => mobile.Android ? 'android' : mobile.iOS ? 'ios' : 'website'
}

function error(data) {

}

function prl(c, n) {
    const l = n > 0 ? n : 150
    let p = c || '-'
    let r = ''
    for (let i = 0; i < l; i++) {
        r += p
    }
    return r
}

const sub = (txt, max) => {
    const lng = max > 10 ? max : 20
    return Vue._.truncate(txt, {length: lng, separator: /,?\.* +/})
}

const go = (obj, route) => {

}

const verify = (response, callback, reject) => {
    let resp = null
    if (response) {
        resp = response.data
        if (is.has(resp, 'code') && resp.code === 777) {
            resp.session_expired = true
            reject(resp.data)
        } else {
            callback(resp.data)
        }
    }
}

const pct = (number, precision = 1) => {
    const factor = Math.pow(10, precision)
    return Math.round(number * factor) / factor
}

const pregquote = (str) => (str + '').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, '\\$1')

const highlight = (needle, haystack) =>
    haystack.replace(
        new RegExp('(' + pregquote(needle) + ')', 'ig'),
        '<span class="highlight">$1</span>'
    )

const currency = (num, cur) =>
    num
        .toFixed(1)
        .replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
        .slice(0, -2) + cur

const userstatus = (stt) => {
    let st = ''
    switch (stt) {
        case 1: {
            st = 'Đang đi làm'
        }
            break
        case 2: {
            st = 'Đang nghỉ'
        }
            break
        case 3: {
            st = 'Đã thôi việc'
        }
            break
    }
    return st
}

function load(obj) {
    let resp = {}
    if (is.obj(obj) && is.has(obj, 'pages')) {
        if (is.obj(obj.pages)) {
            is.for(obj.pages).map(key => {
                let path = is.has(obj, 'path') ? obj.path + '/' : obj.name + '/'
                path = path == 'pages/' ? '' : path
                const item = obj.pages[key]
                path = is.has(item, 'f') ? path + item.f : path + key
                const page = {
                    path: item.p,
                    name: item.n,
                    component: require('../../views/' + path + '/temp').default
                }
                resp[key] = page
                return key
            })
        }
    }
    // log(`Loading ${obj.name}`, resp)
    return resp
}

const _ = (o, k = '', v = null) => {
    let r = null
    if (o && k && typeof o === 'object' && k) {
        r = is.has(o, k) ? o[k] : v
    }
    return v
}

const ufc = s =>
    s.split(' ').map(
        x => (x[0] ? String(x[0]).toUpperCase() + x.slice(1) : '' + x.slice(1))
    ).join(' ')

const jss = o => JSON.stringify(o)

const jsp = s => JSON.parse(s)

function uniless(str) {
    str = str.toLowerCase()
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a')
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e')
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i')
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o')
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u')
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y')
    str = str.replace(/đ/g, 'd')
    return str.toUpperCase()
}

const onlynum = str => str ? str.toString().replace(/\D/g, '') : ''

const checkDateIn = (date, start, end) => {
    let resp = false
    const checkDate = moment(date)
    const startDate = moment(start)
    const endDate = moment(end)
    if (checkDate.isValid() && startDate.isValid() && endDate.isValid()) {
        const chk1 = checkDate.isSameOrAfter(startDate)
        const chk2 = checkDate.isSameOrBefore(endDate)
        if (chk1 === true && chk2 === true) {
            resp = true
        }
    }
    return resp
}

const checkInDaysRange = (date, range = []) => {
    let resp = false
    const checkDate = moment(date)
    if (checkDate.isValid() && range.length) {
        range.forEach(days => {
            const startDate = is.obj(days) && is.has(days, 'start_date') ? moment(days.start_date) : ''
            const endDate = is.obj(days) && is.has(days, 'end_date') ? moment(days.end_date) : ''
            // console.log(`Start: ${days.start_date} - End: ${days.end_date} - Check: ${date}`);
            if (startDate.isValid() && endDate.isValid() && checkDateIn(date, days.start_date, days.end_date)) {
                // console.log(`CHECK RESULT: ${days.start_date} - End: ${days.end_date} - Check: ${date}`);
                resp = true
            }
        })
    }
    return resp
}

function uniq(arr = []) {
    return arr.reduce((a, b) => {
        if (a.indexOf(b) < 0) a.push(b)
        return a
    }, [])
}

function validHolidays(holidays = []) {
    let resp = holidays
    if (resp.length) {
        resp.sort((a, b) => new Date(a.start_date) - new Date(b.start_date))
    }
    return resp
}

function validClassdays(classdays = []) {
    let resp = Array.isArray(classdays) && classdays.length ? classdays : [2, 5]
    if (Array.isArray(resp) && resp.length && resp.length > 1) {
        const buff = []
        resp.map(no => {
            buff.push(parseInt(no.toString().trim()))
            return no
        })
        resp = uniq(buff)
        resp.sort()
        if (resp[0].toString() === "0") {
            resp.shift()
            resp.push(0)
        }
        resp.map(no => parseInt(no.toString().trim()))
    }
    return resp
}

function live(variable) {
    return (typeof variable !== 'undefined');
}

function validFile(file) {
    let resp = {
        is_valid: false,
        message: 'Định dạng file không hợp lệ'
    };
    const validTypes = ["image/png", "image/jpeg", "application/pdf", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword"];
    let pos = validTypes.indexOf(file.type);
    if (pos > -1) {
        resp.is_valid = true;
        resp.message = '';
        switch (pos) {
            case 0:
            case 1:
                resp.icon = 'fa-file-image-o';
                break;
            case 2:
                resp.icon = 'fa-file-pdf-o';
                break;
            default:
                resp.icon = 'fa-file-word-o';
        }
    }

    return resp;
}

const pre = date => moment(date).subtract(1, 'days').format('YYYY-MM-DD');
const nxt = date => moment(date).add(1, 'days').format('YYYY-MM-DD');

const rs = (obj, def = null) => {
    if (typeof obj === 'object' && !Array.isArray(obj)) {
        if (Object.keys(obj).length) {
            Object.keys(obj).forEach(key => {
                obj[key] = rs(obj[key])
            })
        } else obj = {}
    } else if (typeof obj === 'object' && Array.isArray(obj)) {
        obj = []
    } else if (typeof obj === 'number') {
        obj = 0
    } else if (typeof obj === 'boolean') {
        obj = false
    } else if (typeof obj === 'string') {
        obj = obj === 'display' ? 'hidden' : ''
    }
    return def !== null ? def : obj
}

const pr = (str, pat, val) => str && pat && val ? str.toString().replace(`[${pat}]`, val) : str

const set = (obj, val = {}, lim = true) => {
    let resp = obj
    if (is.obj(obj) && val) {
        Object.keys(val).forEach(key => {
            if (lim) {
                if (is.has(obj, key)) {
                    resp[key] = val[key]
                }
            } else resp[key] = val[key]
        })
    } else if (is.arr(obj)) {
        resp.map(o => {
            if (is.obj(o)) {
                const key = Object.keys(o)[0]
                if (is.has(val, key)) {
                    o[key] = val[key]
                }
            }
            return o
        })
    } else {
        resp = val
    }
    return resp
}

const m = alias => {
    return {
        list: {
            html: {
                page: {
                    title: 'Nhấp vào để xem chi tiết',
                    url: {
                        link: `/${alias}/`,
                        apis: `/api/${alias}/`,
                        list: `/api/${alias}/list/`,
                        load: `/api/${alias}/filter/`
                    }
                },
                dom: {},
                data: {},
                search: {
                    keyword: ''
                },
                loading: {
                    action: true,
                    class: false,
                    data: 'Đang tải dữ liệu...',
                    content: 'Đang xử lý...'
                },
                filter: {},
                order: {
                    by: '',
                    to: 'DESC'
                },
                pagination: {
                    url: '',
                    id: '',
                    style: 'line',
                    class: '',
                    spage: 1,
                    ppage: 1,
                    npage: 2,
                    lpage: 2,
                    cpage: 1,
                    total: 2,
                    limit: 20,
                    pages: []
                },
                completed: false
            },
            keys: {},
            vals: {},
            item: {},
            data: {},
            list: [],
            cache: {
                item: {},
                data: {},
                list: []
            },
            temp: {},
            expired: false,
            session: session()
        },
        page: {
            html: {
                page: {
                    title: 'Nhấp vào để xem chi tiết',
                    url: {
                        link: `/${alias}/`,
                        apis: `/api/${alias}/`
                    }
                },
                dom: {},
                loading: {
                    action: true,
                    class: false,
                    content: 'Đang tải dữ liệu...',
                    source: '/static/img/images/loading/mnl_19.gif'
                },
                config: {}
            },
            obj: {},
            keys: {},
            vals: {},
            item: {},
            data: {},
            list: [],
            param: {},
            filter: {},
            cache: {
                dom: {},
                data: {
                    obj: {},
                    item: {},
                    list: []
                }
            },
            temp: {},
            default: {},
            expired: false,
            completed: false,
            session: session()
        }
    }
};

const vld = {
    email: (str) => {
        // const pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
        const pattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
        return pattern.test(str)
    },
    null: (str) => {
        const pattern = /\S+/
        return pattern.test(str)
    },
    num: (str) => {
        // const pattern = /^\d+$/
        const pattern = /^-?\d+\.?\d*$/
        return pattern.test(str)
    },
    cc: (str) => {
        const pattern = /^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|(222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11}|62[0-9]{14})$/
        return pattern.test(str)
    },
    visa: (str) => {
        const pattern = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/
        return pattern.test(str)
    },
    master: (str) => {
        const pattern = /^(?:5[1-5][0-9]{14})$/
        return pattern.test(str)
    },
    amex: (str) => {
        const pattern = /^(?:3[47][0-9]{13})$/
        return pattern.test(str)
    },
    discover: (str) => {
        const pattern = /^(?:6(?:011|5[0-9][0-9])[0-9]{12})$/
        return pattern.test(str)
    },
    same: (str1, str2) => {
        return str1 === str2
    }
}

function fmc(input) {
    let code = ''
    let drap = null
    let resp = {
        s: '',
        n: 0
    }
    if (!input || input.toString() === '' || input.toString() === '0') {
        resp.n = 0
        resp.s = '0'
    } else {
        drap = input.toString().replace(/[\D\s\._\-]+/g, "")
        drap = drap ? parseInt(drap, 10) : 0
        resp.n = drap
        resp.s = drap === 0 ? "0" : `${drap.toLocaleString("en-US")}`
    }
    return resp
}

const convertDateToString = date => date.getFullYear() + "-" + ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + '' + (date.getMonth() + 1))) + "-" + ((date.getDate() > 8) ? date.getDate() : ('0' + '' + (date.getDate())));

const rd = num => {
    let resp = parseInt(num, 10)
    if (num > 999) {
        const tod = num % 1000
        const nod = num / 1000
        if (tod > 0 && tod < 900) {
            resp = nod * 1000
        } else if (tod > 0 && tod > 900) {
            resp = (nod + 1) * 1000
        }
    }
    return resp
}

const formatMoney = input => input.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

const formatNumber = input => parseInt(input.replace(/,/g, ""));

const genErrorHtml = message => `<span class='text-danger'><i class='fa fa-exclamation-circle'></i> ${message}</span></br>`;

const genSuccessHtml = message => `<span class='text-success'>${message}</span></br>`;

function icon(ext) {
    let resp = 'fa-upload';
    switch (ext) {
        case 'jpg':
        case 'png':
            resp = 'fa-file-image-o';
            break;
        case 'pdf':
            resp = 'fa-file-pdf-o';
            break;
        case 'doc':
        case 'docx':
            resp = 'fa-file-word-o';
            break;
        case 'xls':
        case 'xlsx':
            resp = 'fa-file-excel-o';
        default:
            resp = 'fa-upload';
    }
    return resp;
}

function showError(modal, message) {
    modal.message = genErrorHtml(message)
    modal.show = true
}

function showSuccess(modal, message) {
    modal.message = genSuccessHtml(message)
    modal.show = true
}

function showModal(modal, message) {
    modal.message = message
    modal.show = true
}

function setClassDay(transfer_date) {
    return [new Date(transfer_date).getDay()]
}

const subText = (str, len = 25) => {
    let res = str
    if (str.length > len) {
        res = str.substr(0, str.lastIndexOf(' ', len)) + '...'
    }
    return res
}

const getImage = (obj,type) => {
    var link = 'https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?resize=256%2C256&quality=100&ssl=1';
    if(obj == null)return link;
    switch (type) {
        case 'thumbLink': link = `${config.app.service_api}` + obj.thumbLink;break;
        case 'mediumLink': link = `${config.app.service_api}` + obj.mediumLink;break;
        case 'originLink': link = `${config.app.service_api}` + obj.originLink;break;
    }
    return link;
}

const getTextStatusUser = (status) => {
    var str_status = '';
    switch (status) {
        case 0: str_status = 'đang xóa';break;
        case 1: str_status = 'đang hoạt động';break;
    }
    return str_status;
}

const getClassStatusUser = (status) => {
    var str_status = '';
    switch (status) {
        case 0: str_status = 'negative';break;
        case 1: str_status = 'success';break;
    }
    return str_status;
}

export default {
    a,
    c,
    e,
    g,
    d,
    p,
    t,
    m,
    n,
    o,
    v,
    x,
    _,
    ca,
    is,
    lg,
    re,
    rs,
    go,
    pr,
    rd,
    cfg,
    chk,
    md5,
    bus,
    fmc,
    set,
    pct,
    put,
    sub,
    ufc,
    jss,
    jsp,
    log,
    pre,
    nxt,
    vld,
    icon,
    live,
    load,
    print,
    token,
    config,
    verify,
    subText,
    uniless,
    onlynum,
    session,
    currency,
    highlight,
    authorized,
    userstatus,
    checkDateIn,
    validFile,
    formatMoney,
    formatNumber,
    genErrorHtml,
    genSuccessHtml,
    convertDateToString,
    showError,
    showSuccess,
    showModal,
    getImage,
    getClassStatusUser,
    getTextStatusUser,
    baseUrl
};

