import Vue from 'vue'
import App from './App'
import router from './lib/router'
import store from './lib/store'
import Utility from './lib/utilities/utility'
import './lib/utilities/filters'
import './styles/quasar.styl'
//import * as VueGoogleMaps from 'vue2-google-maps'
import 'quasar-framework/dist/quasar.ie.polyfills'
import iconSet from 'quasar-framework/icons/fontawesome'
import 'quasar-extras/animate'
import 'quasar-extras/roboto-font'
import 'quasar-extras/material-icons'
import 'quasar-extras/fontawesome'
import 'quasar-extras/ionicons'
import 'quasar-extras/mdi'
import Quasar, * as All from 'quasar'
import i18n from './lang/i18n'
import VueLocalStorage from 'vue-localstorage'
import mixing from './lib/utilities/mixin'
import LoadScript from 'vue-plugin-load-script';
import VueQrcodeReader from "vue-qrcode-reader";
import QRScanner from 'cordova-plugin-qrscanner';
import VueBarcodeScanner from 'vue-barcode-scanner'
import VueQuagga from 'vue-quaggajs';

let options = {
  sound: true, // default is false
  soundSrc: '/static/sound.wav', // default is blank
  sensitivity: 300, // default is 100
  requiredAttr: true // default is false
}
Vue.use(VueQuagga)
Vue.use(VueBarcodeScanner, options)
Object.defineProperty(Vue.prototype, '$qrscanner', { value: QRScanner });
// import Argon from "./plugins/argon-kit";
Vue.use(VueQrcodeReader);
global.jQuery = require('jquery')
var $ = global.jQuery
window.$ = $

Vue.mixin(mixing)
window.u = Utility
import v from 'vuelidate'
require('dotenv').config()
try {
  window.Vue = require('vue')
  window._ = require('lodash')
  window.m = require('moment')
  Vue.use(v)
  Vue.mixin({ u: Utility });
  // Vue.use(Argon);
} catch (err) {
  console.error(err)
}

Vue.use(LoadScript)

window.axios = require('axios')
// window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
// const token = document.head.querySelector('meta[name='csrf-token']')
// if (token)
//     window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content
// else
//     console.error('CSRF token not found!')

// Vue.loadScript('https://code.jquery.com/jquery-3.3.1.min.js').then(() => {}).catch(() => {})
// Vue.loadScript('https://cdn.jsdelivr.net/npm/quasar@^1.0.0-beta.9/dist/quasar.ie.polyfills.umd.min.js').then(() => {}).catch(() => {})
// Vue.loadScript('https://cdn.jsdelivr.net/npm/quasar@^1.0.0-beta.9/dist/quasar.ie.polyfills.umd.min.js').then(() => {}).catch(() => {})
// Vue.loadScript('https://cdn.jsdelivr.net/npm/vue@latest/dist/vue.min.js').then(() => {}).catch(() => {})
// Vue.loadScript('https://cdn.jsdelivr.net/npm/quasar@^1.0.0-beta.9/dist/quasar.umd.min.js').then(() => {}).catch(() => {})
// Vue.loadScript('https://cdn.jsdelivr.net/npm/quasar@^1.0.0-beta.9/dist/lang/pt-br.umd.min.js').then(() => {}).catch(() => {})
// Vue.loadScript('https://cdn.jsdelivr.net/npm/quasar@^1.0.0-beta.9/dist/icon-set/fontawesome-v5.umd.min.js').then(() => {}).catch(() => {})

Vue.use(Quasar, {
  config: {},
  iconSet: iconSet,
  components: All,
  directives: All,
  plugins: All
});
Vue.use(VueLocalStorage, {
  name: 'ls',
  bind: true //created computed members from your variable declarations
});

Vue.directive('scroll', {
  inserted: function (el, binding) {
    let f = function (evt) {
      if (binding.value(evt, el)) {
        window.removeEventListener('scroll', f)
      }
    }
    window.addEventListener('scroll', f)
  }
})


Vue.prototype.$eventHub = new Vue()

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: h => h(App)
})
