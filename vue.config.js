module.exports = {
  pluginOptions: {
    quasar: {
      theme: 'mat',
      rtlSupport: true,
      importAll: true
    },
    cordovaPath: 'src-cordova'
  },
  transpileDependencies: [/[\\\/]node_modules[\\\/]quasar-framework[\\\/]/],
  outputDir: "cordova/www",
  publicPath: ''
};
